package szachy;

import java.io.Serializable;

/**
 * Created by Agnieszka on 2016-10-23.
 */
public abstract class Figura {

    private String kolor;
    private String[] kolory = {"B", "C"};
    private int X;
    private int Y;

    public void setKolor(int x) {
        kolor = kolory[x];
    }

    public String getKolor() {
        return this.kolor;
    }

    public abstract String getNazwa();


    public void setX(int wczytanyX) {
        if (wczytanyX > 0 && wczytanyX < 9) {
            X = wczytanyX;
        } else {
            System.out.println("Podana współrzędna nie mieści się na planszy.");
        }
    }

    public int getX() {
        return X;
    }


    public void setY(int wczytanyY) {
        if (wczytanyY > 0 && wczytanyY < 9) {
            Y = wczytanyY;
        } else {
            System.out.println("Podana współrzędna nie mieści się na planszy.");
        }
    }

    public int getY() {
        return Y;
    }

    public abstract boolean sprawdzRuch(int X1, int Y1, int X2, int Y2);

}
