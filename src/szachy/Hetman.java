package szachy;

/**
 * Created by Agnieszka on 2016-10-23.
 */
public class Hetman extends Figura implements figuryNieprzeskakujace{
    private String nazwa = "H";
    public String getNazwa() {
        return nazwa;
    }

    public Hetman(int x, int y, int kolor) {
        this.setKolor(kolor);
        this.setX(x);
        this.setY(y);
    }

    @Override
    public boolean sprawdzRuch(int X1,int Y1,int X2, int Y2){
        boolean poprawnyRuch=false;
        if ((Y1==Y2)||(X1==X2)||(X2+Y2)==(X1+Y1)||(X2-Y2)==(X1-Y1)){

            poprawnyRuch=true;
        }
        return (poprawnyRuch);
    }

    @Override
    public boolean sprawdzTorRuchuFigury() {
        boolean czyPrzeskakujeFigure = false;
        //kod sprawdzający

        return czyPrzeskakujeFigure;
    }

}
