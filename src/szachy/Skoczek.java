package szachy;


/**
 * Created by Agnieszka on 2016-10-23.
 */
public class Skoczek extends Figura {
    private String nazwa = "S";
    public String getNazwa() {
        return nazwa;
    }
    public Skoczek (int x, int y, int kolor) {
        this.setKolor(kolor);
        this.setX(x);
        this.setY(y);
    }

    @Override
    public boolean sprawdzRuch(int X1,int Y1,int X2, int Y2){
        boolean poprawnyRuch=false;
        if ((((X2==X1+1)||(X2==X1-1))&&((Y2==Y1+2)||(Y2==Y1-2)))||(((X2==X1+2)||(X2==X1-2))&&((Y2==Y1+1)||(Y2==Y1-1)))){

            poprawnyRuch=true;
        }
        return (poprawnyRuch);
    }


}
