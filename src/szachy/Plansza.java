package szachy;
import java.io.Serializable;

/**
 * Created by Agnieszka on 2016-10-23.
 */
public class Plansza {
    private static String[][]tablica=new String[10][10];
    private static String bialePole = "[ b ]";
    private static String czarnePole= "[ c ]";

    public void wyjsciowaPlansza() {
        int i, j;
        for (i = 1; i < 9; i++) {
            for (j = 1; j < 9; j++) {
                if ((i + j) % 2 == 0) {
                    tablica[i][j] = bialePole;
                } else {
                    tablica[i][j] = czarnePole;
                }
            }
        }
        tablica [0][0]="  ";
        String[] Litery = {"A","B","C","D","E","F","G","H"};
        for (j = 1; j < 9; j++) {
            tablica[0][j] = "  "+Litery[j-1]+"  ";
        }
        for (i = 1; i < 9; i++) {
            tablica[i][0] = Integer.toString(i)+" ";
        }
    }
    public void wyswietlPlansze(){
        int i, j;
        for (i = 0; i < 9; i++) {
            System.out.println();
            for (j = 0; j < 9; j++) {
                System.out.print(tablica[i][j]);
            }
        }
    }

    public void wczytajFigure(String kolorFigury,String nazwaFigury,int X, int Y){
        tablica [X][Y]= "["+kolorFigury+nazwaFigury+" ]";
    }
    public void usunFigure(int X,int Y){
        tablica [X][Y]= "[   ]";
    }

}
