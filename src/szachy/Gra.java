package szachy;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by Agnieszka on 2016-11-11.
 */
public class Gra implements Serializable {
    private boolean koniecGry;
    private boolean ruchCzarnego;

    private static String getUserInput() {
        return sc.nextLine().trim();
    }

    private static Scanner sc = new Scanner(System.in);

    public void start() {
        System.out.println("Zagrajmy w szachy.");
        Plansza plansza = new Plansza();
        plansza.wyjsciowaPlansza();

        List<Figura> listaFigur = new ArrayList<>();
        List<Krol> listaKroli = new ArrayList<>();

        stworzPionki(listaFigur, plansza);
        stworzFiguryGraczaCzarnego(listaFigur, plansza, listaKroli);
        stworzFiguryGraczaBielego(listaFigur, plansza, listaKroli);

        for (int i = 3; i < 7; i++) {
            for (int l = 1; l < 9; l++) plansza.usunFigure(i, l);
        }
        plansza.wyswietlPlansze();

        while (!koniecGry) {

            System.out.println();
            if (!ruchCzarnego) {
                System.out.println("Ruch gracza białego.");
            } else {
                System.out.println("Ruch gracza czarnego.");
            }
            System.out.println("Proszę wpisać ruch w formule: położenie figury, którą chcesz przesunąć + to + docelowe położenie figury. Np: c7toc5. We współrzędnej pola podać najpierw literę, później liczbę.");
            String wczytanyTekst = getUserInput();

            if (czyRuchJestPoprawny(wczytanyTekst)) {
                if (czyMoznaWykonacRuch(konwertujWczytanyTekst(wczytanyTekst), listaFigur)) {
                    Figura figuraWykonujacaRuch = getMeThatFigura(konwertujWczytanyTekst(wczytanyTekst)[0], konwertujWczytanyTekst(wczytanyTekst)[2], listaFigur);
                    boolean testRuchu = figuraWykonujacaRuch.sprawdzRuch(konwertujWczytanyTekst(wczytanyTekst)[0], konwertujWczytanyTekst(wczytanyTekst)[2], konwertujWczytanyTekst(wczytanyTekst)[1], konwertujWczytanyTekst(wczytanyTekst)[3]);
                    if (testRuchu||biciePionkiem(listaFigur,konwertujWczytanyTekst(wczytanyTekst)[1],konwertujWczytanyTekst(wczytanyTekst)[3])) {
                        Figura figura = getMeThatFigura(konwertujWczytanyTekst(wczytanyTekst)[0], konwertujWczytanyTekst(wczytanyTekst)[2], listaFigur);
                        wykonajRuch(figura, plansza, wczytanyTekst);
                        czyZbitoFigure(konwertujWczytanyTekst(wczytanyTekst), listaFigur);
                        ruchCzarnego = !ruchCzarnego;
                    } else {
                        System.out.println("Niedozwolony ruch, przypomnij sobie zasady ruchów figur w szachach.");
                    }
                }
            }
            if (Check(listaKroli, listaFigur)) {
                if (Mate(listaKroli, listaFigur)) {
                    koniecGry = true;
                }
            }

        }
        System.out.println("GAME OVER");
    }

    public boolean biciePionkiem (List <Figura> listaFigur,int docelowePolozenieXPionka, int docelowePolozenieYPionka){
        boolean pionekBije=false;
        for (Figura figura: listaFigur){
            if (figura.getX()==docelowePolozenieXPionka && figura.getY() == docelowePolozenieYPionka){
                pionekBije=true;
                return pionekBije;
            }
        }
        return pionekBije;
    }

    public boolean Check(List<Krol> listaKroli, List<Figura> listaFigur) {
        boolean isCheck = false;
        for (Krol k : listaKroli) {
            int xKrola = k.getX();
            int yKrola = k.getY();
            for (Figura f : listaFigur) {
                if (f.sprawdzRuch(f.getX(), f.getY(), xKrola, yKrola) && !k.getKolor().equals(f.getKolor())) {
                    System.out.println();
                    System.out.println("Szach!");
                    isCheck = true;
                    return isCheck;
                }
            }
        }
        return isCheck;
    }

    public boolean Mate(List<Krol> listaKroli, List<Figura> listaFigur) {
        boolean isMate = false;
        for (Krol k : listaKroli) {
            int xKrola = k.getX();
            int yKrola = k.getY();
            int licznikZajetychPolWokolKrola = 0;
            int licznikFigurKoloruKrola = 0;
            int[] zbior = {3,5,8};
            for (Figura f : listaFigur) {
                if (((f.getX() == (xKrola) || f.getX() == (xKrola - 1) || f.getX() == (xKrola + 1)) && (f.getY() == (yKrola+1) || f.getY() == (yKrola - 1))) || f.getX() == (xKrola - 1) && f.getY() == yKrola || f.getX() == (xKrola + 1) && f.getY() == yKrola) {
                    licznikZajetychPolWokolKrola++;
                    if (k.getKolor().equals(f.getKolor())){
                        licznikFigurKoloruKrola ++;
                    }
                }
            }
            if (((zbior[0]==sprawdzPolozenieKrola(k))||(zbior[1]==sprawdzPolozenieKrola(k))||(zbior[2]==sprawdzPolozenieKrola(k)))&&(licznikZajetychPolWokolKrola==sprawdzPolozenieKrola(k))&&(licznikFigurKoloruKrola==licznikZajetychPolWokolKrola)) {
                String winner = "biały";
                if (k.getKolor().equals("B")) {
                    winner = "czarny";
                }
                System.out.println("Mat! Zwyciężył gracz " + winner);
                isMate = true;
                return isMate;
            }

        }
        return isMate;
    }

    public int sprawdzPolozenieKrola (Krol k){
        int polozenie = 0;
            if ((k.getX()==1||k.getX()==8)&&(k.getY()==1||k.getY()==8)){
                polozenie = 3;
            } else {
                if (((k.getX() == 1 || k.getX() == 8) && (k.getY() != 1 || k.getY() != 8))||((k.getX() != 1 || k.getX() != 8) && (k.getY() == 1 || k.getY() == 8))) {
                    polozenie = 5;
                } else {
                    if ((k.getX()!= 1 && k.getX() != 8) && (k.getY() != 1 && k.getY() != 8)) {
                        polozenie = 8;
                    }
                }
            }
        return polozenie;
    }

    public boolean czyZbitoFigure(int[] wspolrzedne, List<Figura> figury){
        boolean zbitoFigure=false;
            if (wspolrzedne[0]==wspolrzedne[1]&&wspolrzedne[2]==wspolrzedne[3]){
                zbitoFigure =true;
                System.out.println("Przeciwnik zbił Ci figurę.");
                figury.remove(getMeThatFigura(wspolrzedne[1],wspolrzedne[3],figury));
            }
        return zbitoFigure;
    }

    public void wykonajRuch(Figura f, Plansza p, String wczytanyTekst){
        f.setX(konwertujWczytanyTekst(wczytanyTekst)[1]);
        f.setY(konwertujWczytanyTekst(wczytanyTekst)[3]);
        p.wczytajFigure(f.getKolor(), f.getNazwa(), f.getX(), f.getY());
        p.usunFigure(konwertujWczytanyTekst(wczytanyTekst)[0], konwertujWczytanyTekst(wczytanyTekst)[2]);
        p.wyswietlPlansze();

    }

    public boolean czyRuchJestPoprawny(String wczytanyTekst) {
        boolean RuchPoprawny = false;
        if (Pattern.matches("[a-hA-H]{1}[1-8]{1}.*[a-hA-H]{1}[1-8]{1}", wczytanyTekst)) {
            RuchPoprawny = true;
        }
        return RuchPoprawny;
    }

    public int[] konwertujWczytanyTekst(String wczytanyTekst) {
        char x1 = wczytanyTekst.charAt(1);
        char y1 = wczytanyTekst.charAt(0);
        char x2 = wczytanyTekst.charAt(wczytanyTekst.length() - 1);
        char y2 = wczytanyTekst.charAt(wczytanyTekst.length() - 2);
        int[] Wspolrzedne = new int[4];
        int X1 = x1 - '0';
        Wspolrzedne[0] = X1;
        int X2 = x2 - '0';
        Wspolrzedne[1] = X2;
        int Y1 = y1 - 2 * '0';
        Wspolrzedne[2] = Y1;
        int Y2 = y2 - 2 * '0';
        Wspolrzedne[3] = Y2;

        return Wspolrzedne;
    }


    public void stworzPionki(List<Figura> listaFigur, Plansza plansza) {
        int j = 1;
        int k = 1;
        for (int i = 0; i < 16; i++) {
            if (i % 2 == 0) {
                Pionek pionek = new Pionek(2, j, 1);
                plansza.wczytajFigure(pionek.getKolor(), pionek.getNazwa(), pionek.getX(), pionek.getY());
                j++;
                listaFigur.add(pionek);
            } else {
                Pionek pionek = new Pionek(7, k, 0);
                plansza.wczytajFigure(pionek.getKolor(), pionek.getNazwa(), pionek.getX(), pionek.getY());
                k++;
                listaFigur.add(pionek);
            }

        }
    }

    public void stworzFiguryGraczaCzarnego(List<Figura> listaFigur, Plansza plansza, List<Krol> listaKroli) {
        for (int i = 1; i < 9; i++) {
            if ((i == 1) || (i == 8)) {
                Figura figura = new Wieza(1, i, 1);
                dodajAndWczytaj(figura, listaFigur, plansza);
            }
            if (i == 2 || i == 7) {
                Figura figura = new Skoczek(1, i, 1);
                dodajAndWczytaj(figura, listaFigur, plansza);
            }
            if (i == 3 || i == 6) {
                Figura figura = new Goniec(1, i, 1);
                dodajAndWczytaj(figura, listaFigur, plansza);
            }
            if (i == 4) {
                Figura figura = new Hetman(1, i, 1);
                dodajAndWczytaj(figura, listaFigur, plansza);
            }
            if (i == 5) {
                Figura figura = new Krol(1, i, 1);
                dodajAndWczytaj(figura, listaFigur, plansza);
                listaKroli.add( (Krol) figura);
            }

        }
    }

    public void stworzFiguryGraczaBielego(List<Figura> listaFigur, Plansza plansza, List<Krol> listaKroli) {
        for (int i = 1; i < 9; i++) {
            if ((i == 1) || (i == 8)) {
                Figura figura = new Wieza(8, i, 0);
                dodajAndWczytaj(figura, listaFigur, plansza);
            }
            if (i == 2 || i == 7) {
                Figura figura = new Skoczek(8, i, 0);
                dodajAndWczytaj(figura, listaFigur, plansza);
            }
            if (i == 3 || i == 6) {
                Figura figura = new Goniec(8, i, 0);
                dodajAndWczytaj(figura, listaFigur, plansza);
            }
            if (i == 4) {
                Figura figura = new Hetman(8, i, 0);
                dodajAndWczytaj(figura, listaFigur, plansza);
            }
            if (i == 5) {
                Figura figura = new Krol(8, i, 0);
                dodajAndWczytaj(figura, listaFigur, plansza);
                listaKroli.add( (Krol) figura);
            }
        }
    }

    public void dodajAndWczytaj(Figura figura, List<Figura> list, Plansza plansza) {
        list.add(figura);
        plansza.wczytajFigure(figura.getKolor(), figura.getNazwa(), figura.getX(), figura.getY());
    }

    public Figura getMeThatFigura(int x, int y, List<Figura> figuras) {
        for (Figura figura : figuras) {
            if (figura.getX() == x && figura.getY() == y) {
                return figura;
            }
        }
        return null;
    }

    public boolean czyMoznaWykonacRuch(int[] wspolrzedne, List<Figura> figury) {
        boolean moznaWykonacRuch = false;
        boolean pierwszaFiguraToFiguraGracza = false;
        boolean drugaFiguraToFiguraPrzeciwnika = false;
        boolean wspolrzednenapustepole = false;
        for (Figura f : figury) {

            int xSzukanejFiguryGracza = f.getX();
            int ySzukanejFiguryGracza = f.getY();
            Figura figuraGracza = getMeThatFigura(xSzukanejFiguryGracza, ySzukanejFiguryGracza, figury);
            String kolor = figuraGracza.getKolor();
            if ((xSzukanejFiguryGracza == wspolrzedne[0] && ySzukanejFiguryGracza == wspolrzedne[2]) && (kolor.equals("C") && ruchCzarnego || kolor.equals("B") && !ruchCzarnego)) {
                pierwszaFiguraToFiguraGracza = true;
            }
        }
        for (Figura f1 : figury) {
            int xSzukanejFiguryPrzeciwnika = f1.getX();
            int ySzukanejFiguryPrzeciwnika = f1.getY();
            Figura figuraPrzeciwnika = getMeThatFigura(xSzukanejFiguryPrzeciwnika, ySzukanejFiguryPrzeciwnika, figury);
            String kolor = figuraPrzeciwnika.getKolor();
            if ((xSzukanejFiguryPrzeciwnika == wspolrzedne[1] && ySzukanejFiguryPrzeciwnika == wspolrzedne[3]) && (kolor.equals("C") && !ruchCzarnego || kolor.equals("B") && ruchCzarnego)) {
                drugaFiguraToFiguraPrzeciwnika = true;
            } else {
                if ((xSzukanejFiguryPrzeciwnika == wspolrzedne[1] && ySzukanejFiguryPrzeciwnika == wspolrzedne[3]) && (kolor.equals("C") && ruchCzarnego || kolor.equals("B") && !ruchCzarnego)) {
                    System.out.println("Nie możesz zbić własnej figury");
                    return moznaWykonacRuch;
                } else {
                    wspolrzednenapustepole = true;
                }
                }
            }
        if ((pierwszaFiguraToFiguraGracza && drugaFiguraToFiguraPrzeciwnika) || (pierwszaFiguraToFiguraGracza && wspolrzednenapustepole)) {
            moznaWykonacRuch = true;

        }
        return moznaWykonacRuch;

        }
        /*public void zapiszStanGrySerializacja(Gra gra){
            try {
                FileOutputStream strumienPlk = new FileOutputStream("MojaGra.ser");
                ObjectOutputStream so = new ObjectOutputStream(strumienPlk);
                so.writeObject(gra);
                so.close();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        public void wczytajStanGrySerializacja (){
            try{
            ObjectInputStream os = new ObjectInputStream(new FileInputStream("MojaGra.ser"));
            Gra wczytanaGra= (Gra)os.readObject();
            os.close();
            }catch(FileNotFoundException f) {
                System.out.println("Nie znaleziono pliku.");
                f.printStackTrace();
            }catch(Exception e) {
                e.printStackTrace();
            }
        }*/


}





