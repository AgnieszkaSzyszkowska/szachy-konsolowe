package szachy;

/**
 * Created by Agnieszka on 2016-10-23.
 */
public class Pionek extends Figura implements figuryNieprzeskakujace{
    private String nazwa = "P";
    public String getNazwa() {
        return nazwa;
    }
    private int iloscRuchowPionka;
    public Pionek(int x, int y, int kolor) {
        this.setKolor(kolor);
        this.setX(x);
        this.setY(y);
    }

    @Override
    public boolean sprawdzRuch(int X1,int Y1,int X2, int Y2){
        boolean poprawnyRuch=false;
        if ((Y1==Y2)&&((X1+1)==X2||(X1-1)==X2)){
            poprawnyRuch=true;
            iloscRuchowPionka++;
        } else {
            if (iloscRuchowPionka<1&&((Y1==Y2)&&((X1+2)==X2||(X1-2)==X2))){
            poprawnyRuch=true;
            iloscRuchowPionka++;
        }
        }
        return (poprawnyRuch);
    }

    @Override
    public boolean sprawdzTorRuchuFigury() {
        boolean czyPrzeskakujeFigure = false;
        //kod sprawdzający

        return czyPrzeskakujeFigure;
    }

}

