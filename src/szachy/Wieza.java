package szachy;

/**
 * Created by Agnieszka on 2016-10-23.
 */
public class Wieza extends Figura implements figuryNieprzeskakujace {
    private String nazwa = "W";
    public String getNazwa() {
        return nazwa;
    }

    public Wieza(int x, int y, int kolor) {
        this.setKolor(kolor);
        this.setX(x);
        this.setY(y);
    }

    @Override
    public boolean sprawdzRuch(int X1, int Y1, int X2, int Y2){
        boolean poprawnyRuch=false;
        if ((Y1==Y2)||(X1==X2)){
            poprawnyRuch=true;
        }
        return (poprawnyRuch);
    }

    @Override
    public boolean sprawdzTorRuchuFigury() {
        boolean czyPrzeskakujeFigure = false;
        //kod sprawdzający

        return czyPrzeskakujeFigure;
    }
}


